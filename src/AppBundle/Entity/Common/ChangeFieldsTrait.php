<?php

namespace AppBundle\Entity\Common;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangeFieldsTrait
 */
trait ChangeFieldsTrait
{
    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $createDate;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $updateDate;

    /**
     * @return DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param DateTime $createDate
     *
     * @return $this
     */
    public function setCreateDate(DateTime $createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param DateTime $updateDate
     *
     * @return $this
     */
    public function setUpdateDate(DateTime $updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }
}

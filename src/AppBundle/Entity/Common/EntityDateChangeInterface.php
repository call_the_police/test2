<?php

namespace AppBundle\Entity\Common;

use DateTime;

interface EntityDateChangeInterface
{
    /**
     * @return DateTime
     */
    public function getCreateDate();

    /**
     * @param DateTime $createDate
     *
     * @return $this
     */
    public function setCreateDate(DateTime $createDate);


    /**
     * @return DateTime
     */
    public function getUpdateDate();

    /**
     * @param DateTime $updateDate
     *
     * @return $this
     */
    public function setUpdateDate(DateTime $updateDate);
}

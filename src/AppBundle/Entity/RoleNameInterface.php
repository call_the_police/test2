<?php

namespace AppBundle\Entity;

/**
 * Interface RoleNameInterface
 */
interface RoleNameInterface
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
}

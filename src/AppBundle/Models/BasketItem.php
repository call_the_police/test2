<?php

namespace AppBundle\Models;

use AppBundle\Entity\Product;

/**
 * Class BasketItem
 */
class BasketItem
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * BasketItem constructor.
     *
     * @param Product $product
     * @param int     $count
     */
    public function __construct(Product $product, int $count = 1)
    {
        $this->product = $product;
        $this->quantity = $count;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     *
     * @return $this
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return $this
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }
}

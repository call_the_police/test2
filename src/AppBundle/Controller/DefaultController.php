<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\PersonalLoginForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class DefaultController
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function personalAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function personalLoginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $form = $this->createForm(PersonalLoginForm::class);

        if ($request->isMethod(Request::METHOD_POST)) {
            $form->handleRequest($request);
            if ($form->isValid() && is_null($error)) {
                return $this->redirectToRoute('index');
            }
        }

        return $this->render(':default:login.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
        ]);
    }

    /**
     * @param Request $request
     */
    public function personalCheckAction(Request $request)
    {
        //empty method just to start security services
    }


    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function adminAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Security("has_role('ROLE_USER')")
     */
    public function adminLoginAction(Request $request)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function logoutAction(Request $request)
    {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return $this->redirectToRoute('index');
    }


    public function testAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $productRepository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product1 = $productRepository->find(1);
        $product2 = $productRepository->find(2);

        $basket = $this->get('basket.service')->addToBasket($product1, 2);
        $basket = $this->get('basket.service')->addToBasket($product1, 1);

        return $this->render('default/index.html.twig');
    }
}

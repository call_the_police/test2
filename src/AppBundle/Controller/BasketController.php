<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Basket;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class BasketController extends Controller
{
    /**
     * @param Product $product
     * @param int $count
     *
     * @return Response
     */
    public function addAction(Product $product, $count = 1)
    {
        /** @var User $user */
        $user = $this->getUser();
        $basketItem = (new Basket())->setProduct($product)->setUser($user)->setQuantity($count);

        $token = $this->getTokenStorage()->getToken();
        $token->setUser($user->addToBasket($basketItem));
        $this->getDoctrine()->getManager()->persist($basketItem);
        $this->getDoctrine()->getManager()->flush();
        $this->getTokenStorage()->setToken($token);

        return new Response('OK');
    }

    /**
     * @return object|TokenStorage
     */
    private function getTokenStorage()
    {
        return $this->container->get('security.token_storage');
    }


}

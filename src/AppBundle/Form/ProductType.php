<?php

namespace AppBundle\Form;

use AppBundle\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Validator\Constraints\LessThanOrEqual;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vendorCode')
            ->add('active')
            ->add(
                'sort',
                IntegerType::class,
                [
                    'constraints' => [
                        new GreaterThanOrEqual(['value' => 1]),
                        new LessThanOrEqual(['value' => 1000]),
                    ]
                ]
            )
            ->add('name')
            ->add('description')
            ->add('code')
            ->add(
                'price',
                IntegerType::class,
                [
                    'constraints' => [
                        new GreaterThanOrEqual(['value' => 0]),
                    ]
                ]
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Product::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'product';
    }
}

<?php

namespace AppBundle\Security\Authentication;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

/**
 * Class AuthenticationFailureHandler
 */
class AuthenticationFailureHandler implements AuthenticationFailureHandlerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var HttpKernelInterface
     */
    protected $httpKernel;

    /**
     * @var HttpUtils
     */
    protected $httpUtils;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var
     */
    protected $options;

    /**
     * @var array
     */
    protected $defaultOptions = [
        'failure_path' => null,
        'failure_forward' => false,
        'login_path' => '/login',
        'failure_path_parameter' => '_failure_path',
    ];

    /**
     * Constructor.
     *
     * @param HttpKernelInterface $httpKernel
     * @param HttpUtils           $httpUtils
     * @param array               $options    Options for processing a failed authentication attempt.
     */
    public function __construct(
        HttpKernelInterface $httpKernel,
        HttpUtils $httpUtils,
        array $options = []
    ) {
        $this->httpKernel = $httpKernel;
        $this->httpUtils = $httpUtils;
        $this->setOptions($options);
    }

    /**
     * @return array An array of options
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options An array of options
     */
    public function setOptions(array $options)
    {
        $this->options = array_merge($this->defaultOptions, $options);
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response The response to return, never null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if ($failureUrl = $request->get($this->options['failure_path_parameter'])) {
            $this->options['failure_path'] = $failureUrl;
        }

        if (null === $this->options['failure_path']) {
            $this->options['failure_path'] = $this->options['login_path'];
        }

        if ($this->options['failure_forward']) {
            if (null !== $this->logger) {
                $this->logger->debug(
                    'Authentication failure, forward triggered.',
                    ['failure_path' => $this->options['failure_path']]
                );
            }

            $subRequest = $this->getCopyRequestToPath($request, $this->options['failure_path']);
            $subRequest->attributes->set(Security::AUTHENTICATION_ERROR, $exception);

            return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
        }

        if (null !== $this->logger) {
            $this->logger->debug(
                'Authentication failure, redirect triggered.',
                ['failure_path' => $this->options['failure_path']]
            );
        }

        $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);

        return $this->httpUtils->createRedirectResponse($request, $this->options['failure_path'], 307);
    }

    /**
     * @param Request $request
     * @param string  $path
     *
     * @return Request
     */
    private function getCopyRequestToPath(Request $request, string $path)
    {
        $newRequest = Request::create(
            $this->httpUtils->generateUri($request, $path),
            $request->getMethod(),
            $request->request->all(),
            $request->cookies->all(),
            [],
            $request->server->all()
        );

        if ($request->hasSession()) {
            $newRequest->setSession($request->getSession());
        }

        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $newRequest->attributes->set(
                Security::AUTHENTICATION_ERROR,
                $request->attributes->get(Security::AUTHENTICATION_ERROR)
            );
        }
        if ($request->attributes->has(Security::ACCESS_DENIED_ERROR)) {
            $newRequest->attributes->set(
                Security::ACCESS_DENIED_ERROR,
                $request->attributes->get(Security::ACCESS_DENIED_ERROR)
            );
        }
        if ($request->attributes->has(Security::LAST_USERNAME)) {
            $newRequest->attributes->set(Security::LAST_USERNAME, $request->attributes->get(Security::LAST_USERNAME));
        }

        return $newRequest;
    }
}
